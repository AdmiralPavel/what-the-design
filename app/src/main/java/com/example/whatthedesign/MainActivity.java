package com.example.whatthedesign;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private int PICK_IMAGE_REQUEST = 1;
    private Bitmap bitmap;
    private String path;

    private void showInfo() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage(R.string.info).setTitle("Info").setPositiveButton("Ok", (dialog, which) -> {
        });
        builder.setTitle("Info");
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.mainmenu, menu);
        return true;
    }

    private void uploadToServer(String filePath) {
        ProgressBar progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(ProgressBar.VISIBLE);
        // Map is used to multipart the file using okhttp3.RequestBody
        File file = new File(filePath);

        // Parsing any Media type file
        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), requestBody);
        RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());
        Button button = findViewById(R.id.button);
ImageView imageView = findViewById(R.id.imageView);
        ImageUploadApi getResponse = NetworkService.getRetrofitClient().create(ImageUploadApi.class);
        Call call = getResponse.upload(fileToUpload, filename);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                    TextView textView = findViewById(R.id.resultTextView);
                    imageView.setEnabled(true);
                button.setEnabled(true);
                progressBar.setVisibility(ProgressBar.INVISIBLE);
                try {
                    textView.setText("Похоже, что это стиль \"" + ((ResponseBody) response.body()).string() + "\"");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                button.setEnabled(true);
                imageView.setEnabled(true);
                progressBar.setVisibility(ProgressBar.INVISIBLE);
                Toast toast = Toast.makeText(getApplicationContext(), "Failure! Check your internet connection", Toast.LENGTH_SHORT);
                toast.show();
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        showInfo();
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ImageView imageView = findViewById(R.id.imageView);
        Button button = findViewById(R.id.button);
        button.setOnClickListener((v) -> {
            button.setEnabled(false);
            imageView.setEnabled(false);
            uploadToServer(path);
        });
        imageView.setOnClickListener((v) -> {
            imageView.setEnabled(false);
            EasyImage.openGallery(MainActivity.this, 1);
            imageView.setEnabled(true);
        });
        showInfo();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                ImageView imageView = findViewById(R.id.imageView);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap bitmap = BitmapFactory.decodeFile(imageFiles.get(0).getAbsolutePath(), options);
                imageView.setImageBitmap(bitmap);
                Button button = findViewById(R.id.button);
                button.setVisibility(View.VISIBLE);
                button.setEnabled(true);
                path = imageFiles.get(0).getPath();
            }

        });
    }
}
